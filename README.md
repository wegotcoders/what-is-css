# Cats CSS Selector Practice #

Using CSS to target and style elements with CSS Selectors.

### Clone Repository ###

1. Fork this repository.
2. Git clone your own copy to your assignments folder.
3. Change directory into your local copy of the repo.

### To Do ###

1. Open your local copy folder with Sublime, there is a HTML document, a CSS style sheet and an image file.
2. Use your knowledge of CSS selectors to target and style elements in the HTML document.
3. Add your style rules to style.css, this file is linked to from cats.html.
4. Try to use all the CSS selectors you have learned to style all elements on the page.
5. You can see your progress by typing in terminal 'open cats.html'.
